from django.apps import AppConfig


class DjangoAnyJSConfig(AppConfig):
    name = 'django_any_js'
